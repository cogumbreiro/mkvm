import tarfile
import shutil

from StringIO import StringIO

def tar_filter(tarin, tarout, filter_func):
    """
    Copies members from 'tarin' to 'tarout'.
    For each member, we call 'filter_func(tar_info, fd)', where 'tar_info'
    is the info of the member and 'fd' is fileobj of the member.
    Members are skipped whenever 'filter_func' returns 'None'.
    Remember to update the information (e.g. the size) of TarInfo if the
    fileobj is altered. Also note that if 'tarin' is read in stream mode,
    then the fileobj supplied to 'filter_func' cannot be seeked.
    
     - tarin: TarFile
     - tarout: TarFile
     - filter_func(TarInfo, fileobj) -> fileobj
    """
    for info in tarin:
        fd = filter_func(info, tarin.extractfile(info))
        if fd is not None:
            tarout.addfile(info, fd)

def tar_update(tarin, tarout, update_func, needs_update_func):
    """
    Updates the contents of 'tarin' and outputs the effects to 'tarout'.
    
    If 'needs_update_func(tar_info)' returns false, then the element is copied
    verbatim, otherwise the info and the fileobj is supplied to 'update_func'.
    
    The fileobj is 
    
    Updates a member of a tar file. Takes care of creating a temporary that
    fileobj to be supplied to the update_func.
    Function update_func is only called if needs_update_func returns true.
    
     - tarin: fileobj
     - tarout: fileobj
     - update_func(info, fd) -> StringIO
     - needs_update_func(info) -> bool
    """
    def update(info, fd):
        # check if we need to update
        if not needs_update_func(info):
            return fd
        # make a temporary buffer, so we can alter its contents
        tmp_buff = StringIO()
        shutil.copyfileobj(fd, tmp_buff)
        tmp_buff.seek(0)
        # get the new stream
        fdout = update_func(info, tmp_buff)
        fdout.seek(0)
        # write the contents back to the stream
        info.size = len(bytearray(fdout.getvalue()))
        fdout.seek(0)
        return fdout
        
    tar_filter(tarin, tarout, update)
    
def tar_rename(tarin, tarout, renames):
    """
    Renames multiple filenames in a tar. Uses the archive from 'tarin' and
    places the effects in 'tarout'. The renames are given in dictionary
    'renames'.
    """
    def update(info, fd):
        info.name = renames[info.name]
        return fd
        
    def needs_update(info):
        return info.name in renames
        
    tar_update(tarin, tarout, src_filename, update)


