from UserDict import DictMixin

class DiskImages(DictMixin):
    def __init__(self, fp):
        self._contents = None
        self._fp = fp
        self._renames = dict()

    def _check(self, name, msg):
        if name in self._renames.values() or src in self._renames:
            raise ValueError("%s: '%s' already in renames." % (msg, name))
    

    def rename(self, src, dst):
        msg = "Could not rename %r to %r" % (src, dst)
        self._check(src, msg)
        self._check(dst, msg)        
        self._renames[src] = dst

    def write(fdout):
        with tarfile.open(fileobj=self._fp, mode="r|") as tarin:
            with tarfile.open(fileobj=fdout, mode="w|") as tarout:
                tar_rename(tarin, tarout, self._renames)

def parse(fp):
    return DiskImages(fp)

