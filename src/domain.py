"""
Minimal manipulation of domain objects.
"""

from xml.etree import ElementTree as ET

def get_disk_sources(root):
    """
    Returns all source attributes that target a file.
    """
    return root.findall(".//disk[@type='file']//source[@file]")

class DiskImages(object):
    """
    Represents a collection of disk image files.
    """
    def __init__(self, xml):
        self.xml = xml
        self.data = list(get_disk_sources(xml))

    def __getitem__(self, key):
        return self.data.attrib['file']
    
    def __setitem__(self, key, value):
        self.data[key].attrib['file'] = value
    
    def _get_files(self):
        for source in self.data:
            yield source.attrib['file']
            
    def __iter__(self):
        return iter(self._get_files())
    
    def __len__(self):
        return len(self.data)
    
    def __repr__(self):
        return repr(list(self._get_files()))
    
class Domain(object):
    def __init__(self, xml):
        self.xml = xml
        self._name = xml.find("./name")
        self.disk_images = DiskImages(xml)
        self.write = xml.write
    
    @property
    def name(self):
        return self._name.text
        
    @name.setter
    def name(self, value):
        self._name.text = value
    
    def __repr__(self):
        return "Domain(name=%r, disk_images=%r)" % (self.name, self.disk_images)
    
def parse(fd):
    return Domain(ET.parse(fd))
    
def fromstring(data):
    return Domain(ET.fromstring(data))


